install:
	pip3 install -U pip && pip3 install -r requirements.txt
	
lint:
	pylint --disable=R,C app.py
	
all: install lint